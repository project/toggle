<?php

/**
 * @file
 * Toggle module.
 */

use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;


/**
 * Implements hook_element_info().
 */
function toggle_element_info() {
  $elements = array();

  $elements['toggle_radios'] = array(
    '#input' => TRUE,
    '#process' => array(
      'form_process_radios',
      'toggle_process_radios',
    ),
    '#pre_render' => array('form_pre_render_conditional_form_element'),
    '#theme_wrappers' => array('toggle_radios'),
  );

  $elements['toggle_radio'] = array(
    '#input' => TRUE,
    '#default_value' => NULL,
    '#process' => array('ajax_process_form'),
    '#pre_render' => array(
      'form_pre_render_radio',
      'toggle_pre_render_radio'
    ),
    '#theme' => 'toggle_radio',
    '#theme_wrappers' => array('form_element'),
    '#title_display' => 'after',
  );

  $elements['toggle_checkboxes'] = array(
    '#input' => TRUE,
    '#process' => array(
      'form_process_checkboxes',
      'toggle_process_checkboxes',
    ),
    '#pre_render' => array('form_pre_render_conditional_form_element'),
    '#theme_wrappers' => array('toggle_checkboxes'),
  );

  $elements['toggle_checkbox'] = array(
    '#input' => TRUE,
    '#return_value' => 1,
    '#process' => array('form_process_checkbox', 'ajax_process_form'),
    '#pre_render' => array(
      'form_pre_render_checkbox',
      'toggle_pre_render_checkbox',
    ),
    '#theme' => 'toggle_checkbox',
    '#theme_wrappers' => array('form_element'),
    '#title_display' => 'after',
  );

  return $elements;
}

/**
 * Implements hook_theme().
 */
function toggle_theme() {
  return array(
    'toggle_radios' => array(
      'render element' => 'element',
      'file' => 'toggle.theme.inc',
    ),
    'toggle_radio' => array(
      'render element' => 'element',
      'file' => 'toggle.theme.inc',
    ),
    'toggle_checkboxes' => array(
      'render element' => 'element',
      'file' => 'toggle.theme.inc',
    ),
    'toggle_checkbox' => array(
      'render element' => 'element',
      'file' => 'toggle.theme.inc',
    ),
  );
}

/**
 * Prepares a #type 'toggle_checkbox' render element for theme_toggle_checkbox().
 * Since we're also running our element through form_pre_render_checkbox(),
 * there's not much left to do other than add our class.
 *
 * @param array $element
 *   An associative array containing the properties of the element.
 *   Properties used: #title, #value, #return_value, #description, #required,
 *   #attributes, #checked.
 *
 * @return array
 *   The $element with prepared variables ready for theme_toggle_checkbox().
 *
 * @see form_pre_render_checkbox()
 */
function toggle_pre_render_checkbox($element) {
  $element['#attributes']['class'][] = 'form-toggle';
  $element['#attributes']['class'][] = 'checkbox';

  // Add a class for single boolean fields.
  if (!array_key_exists('#multiple', $element) || !$element['#multiple']) {
    $element['#attributes']['class'][] = 'single';
  }
  // Add a dummy empty title and hide it for elements that have no title. This
  // is necessary because of the way we theme our toggles. Adding this empty
  // element here, as well as adding the duplicate one and hiding with the
  // "single" class, above, is probably not the best thing for Accessibility or
  // SEO, I'm not really sure. If anyone hates this, please open an issue, thanks!
  else if (empty($element['#title'])) {
    $element['#title'] = '&nbsp;';
    $element['#attributes']['class'][] = 'single';
  }

  return $element;
}

/**
 * Prepares a #type 'toggle_radio' render element for theme_toggle_radio().
 * Since we're also running our element through form_pre_render_radio(), there's
 * not much left to do other than add our class.
 *
 * @param array $element
 *   An associative array containing the properties of the element.
 *   Properties used: #title, #value, #return_value, #description, #required,
 *   #attributes, #checked.
 *
 * @return array
 *   The $element with prepared variables ready for theme_toggle_radio().
 *
 * @see form_pre_render_radio()
 */
function toggle_pre_render_radio($element) {
  $element['#attributes']['class'][] = 'form-toggle';
  $element['#attributes']['class'][] = 'radio';
  return $element;
}

/**
 * Processes a "toggle_checkboxes" form element. We run our element through
 * form_process_checkboxes() and then just modify the children to be toggle
 * elements, this way core does all the heavy lifting.
 *
 * @see form_process_checkboxes()
 */
function toggle_process_checkboxes($element) {
  foreach (element_children($element) as $key) {
    $element[$key]['#type'] = 'toggle_checkbox';
    $element[$key]['#multiple'] = TRUE;
  }
  return $element;
}

/**
 * Processes a "toggle_radios" form element. We run our element through
 * form_process_radios() and then just modify the children to be toggle
 * elements, this way core does all the heavy lifting.
 *
 * @see form_process_radios()
 */
function toggle_process_radios($element) {
  foreach (element_children($element) as $key) {
    $element[$key]['#type'] = 'toggle_radio';
  }
  return $element;
}

/**
 * Determines the value for a toggle_checkbox form element. We don't do anything
 * special here so just use core checkbox value callback.
 */
function form_type_toggle_checkbox_value($element, $input = FALSE) {
  return form_type_checkbox_value($element, $input);
}

/**
 * Determines the value for a toggle_checkboxes form element. We don't do
 * anything special here so just use core checkboxes value callback.
 */
function form_type_toggle_checkboxes_value($element, $input = FALSE) {
  return form_type_checkboxes_value($element, $input);
}

/**
 * Determines the value for a toggle_radios form element. We don't do anything
 * special here so just use core radios value callback.
 */
function form_type_toggle_radios_value(&$element, $input = FALSE) {
  return form_type_radios_value($element, $input);
}

/**
 * Implements hook_field_widget_info_alter().
 *
 * Enable Toggle field widget for entity and term reference fields, if the
 * respective modules are enabled.
 */
function toggle_field_widget_info_alter(&$info) {
  if (Drupal::moduleHandler()->moduleExists('taxonomy')) {
    $info['options_toggle']['field_types'][] = 'taxonomy_term_reference';
  }
  if (Drupal::moduleHandler()->moduleExists('entity_reference')) {
    $info['options_toggle']['field_types'][] = 'entity_reference';
  }
}

/**
 * Implements hook_help().
 */
function toggle_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.toggle':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Toggle module allows you to create fields where data values are selected using a slick on-off Toggle widget. See the <a href="!field">Field module help</a> and the <a href="!field_ui">Field UI help</a> pages for general information on fields and how to create and manage them. For more information, see the <a href="!toggle_do">online documentation for the Toggle module</a>.', array('!field' => Url::fromRoute('help.page', array('name' => 'field'))->toString(), '!field_ui' => Url::fromRoute('help.page', array('name' => 'field_ui')), '!toggle_do' => 'https://drupal.org/project/toggle')) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Managing and displaying toggle fields') . '</dt>';
      $output .= '<dd>' . t('The <em>settings</em> and the <em>display</em> of the toggle fields can be configured separately. See the <a href="!field_ui">Field UI help</a> for more information on how to manage fields and their display.', array('!field_ui' => Url::fromRoute('help.page', array('name' => 'field_ui')))) . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}
