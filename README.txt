README.txt
==========

This module defines a "Toggle" field formatter and widget for boolean and list
fields.

This started off as a Drupal 8.x port of the Drupal 7.x iToggle (https://drupal.org/project/itoggle)
module but the idea now is to start from scratch and offer something a lot more
generic.


COMPATIBILITY
===

This module is being developed for Drupal 8.0-alpha3. The chances of something
breaking with any other Drupal version is pretty high. I'll do my best to update
this whenever there's a new official Drupal 8 release.

Thanks!

- Alex
